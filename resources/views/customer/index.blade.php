<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Customer Blog</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
 
 <style>
   .container{
    padding: 0.5%;
   } 
   .error{ color:red; } 
</style>
</head>
<body>
@include('layouts.app')
<div class="container">
    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-customer"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Customer</a> 
          
          <table class="table table-bordered" id="laravel_crud">
           <thead>
              <tr>
              <th>#</th>
                <th>Name</th>
                <th>E-mail</th>
                <th>Profile</th>
                <th>Mobile</th>
                <th>Gender</th>
                <th>Hobbies</th>
                <th>Address</th>
                <th>City</th>
                <th>State</th>
                <td colspan="2"><b>Action</b></td>
              </tr>
           </thead>
           <tbody id="customers-crud">
              @foreach($customers as $customer)
              <tr id="customer_id_{{ $customer->id }}">
                 <td>{{ ++$i }}</td>
                 <td>{{ $customer->name  }}</td>
                 <td>{{ $customer->email }}</td>
                 <td><img style="margin-left:20px;border-radius: 50%;" src="/profile_images/{{ $customer->profile }}" width="40" height="40" /></td>
                 <td>{{ $customer->mobile }}</td>
                 <td>{{ $customer->gender }}</td>
                 <td>{{ $customer->hobbies }}</td>
                 <td>{{ $customer->address }}</td>
                 <td>{{ $customer->city }}</td>
                 <td>{{ $customer->state }}</td>
                 <td><a href="javascript:void(0)" id="edit-customer" data-id="{{ $customer->id }}" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                 <td>
                  <a href="javascript:void(0)" id="delete-customer" data-id="{{ $customer->id }}" class="btn btn-danger delete-customer"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
              </tr>
              @endforeach
           </tbody>
          </table>
          {{ $customers->links() }}
       </div> 
    </div>
</div>
<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="CustomCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="customerForm" name="customerForm" class="form-horizontal" enctype="multipart/form-data">
        @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success d-none" id="msg_div">
                            <span id="res_message"></span>
                        </div>
                </div>
            </div>
           <input type="hidden" name="customer_id" id="customer_id">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" value="">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>
            </div>
 
            <div class="form-group">
                <label class="col-sm-2 control-label">E-Mail</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="" >
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-12">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" value="" >
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Mobile</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile Number" value="" >
                    <span class="text-danger">{{ $errors->first('mobile') }}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Profile</label>
                <div class="col-sm-12">
                    <input type="file" class="form-control" name="profile" placeholder="Choose image" id="profile">
                    <span class="text-danger">{{ $errors->first('profile') }}</span>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label">Gender</label>


                <div class="col-sm-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="gender" id="gender" value="Male">Male
                        </label>
                        <label>
                            <input type="radio" name="gender" id="gender" value="Female">Female
                        </label>
                    </div>
                    <span class="text-danger">{{ $errors->first('gender') }}</span>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label">Hobbies</label>
                <div class="col-sm-12">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="hobbies[]" id="hobbies" value="Cricket">Cricket
                        </label>
                        <label>
                            <input type="checkbox" name="hobbies[]" id="hobbies" value="Bike Ride">Bike Ride
                        </label>
                        <label>
                            <input type="checkbox" name="hobbies[]" id="hobbies" value="Book Reading">Book Reading
                        </label>
                        <span class="text-danger">{{ $errors->first('hobbies') }}</span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Address</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="" >
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label">City</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="city" name="city" placeholder="Enter City" value="" >
                    <span class="text-danger">{{ $errors->first('city') }}</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">State</label>
                <div class="col-sm-12">
                    <select name="state" id="state">
                        <option value="" selected disabled>Choose here</option>
                        <option value="TamilNadu">Tamil Nadu</option>
                        <option value="Karanataka">Karanataka</option>
                        <option value="Hyderabad">Hyderabad</option>
                    </select>
                    <span class="text-danger">{{ $errors->first('state') }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button type="submit" id="btn-save" class="btn btn-block btn-success">Submit</button>
                </div>   
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>
</body>
</html>
<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#create-new-customer').click(function () {
        $('#btn-save').val("create-customer");
        $('#customerForm').trigger("reset");
        $('#CustomCrudModal').html("Add New Customer");
        $('#ajax-crud-modal').modal('show');
    });
 
    $('body').on('click', '#edit-customer', function () {
      var customer_id = $(this).data('id');
      $.get("{{ route('customers.index') }}" +'/' + customer_id +'/edit', function (data) {
         $('#CustomCrudModal').html("Edit customer");
          $('#btn-save').val("edit-customer");
          $('#ajax-crud-modal').modal('show');
          $('#customer_id').val(data.id);
          $('#name').val(data.name);
          $('#email').val(data.email);
          $('#password').val(data.password);
          $('#mobile').val(data.mobile);
          $('#profile').val(data.profile);
          $('#gender').val(data.gender);
          $('#hobbies').val(data.hobbies);
          $('#address').val(data.address);
          $('#city').val(data.city);
          $('#state').val(data.state);  
      })
   });

    $('body').on('click', '.delete-customer', function () {
        var customer_id = $(this).data("id");
        confirm("Are You sure want to delete !");
 
        $.ajax({
            type: "DELETE",
            url: "{{ url('customers')}}"+'/'+customer_id,
            success: function (data) {
                $("#customer_id_" + customer_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });   
  });
 
 if ($("#customerForm").length > 0) {
      $("#customerForm").validate({

    rules: {
      name: {
        required: true,
        maxlength: 20
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 6
      },
      mobile: {
        required: true,
        maxlength: 10
      },
      profile: {
        required: true
      },
      gender: {
        required: true
      },
      hobbies: {
        required: true
      },
      address: {
        required: true
      },
      city: {
        required: true
      },
      state: {
        required: true
      },
    },
    messages: {
      name: {
        required: "Please Enter Name",
        maxlength: "Your last name maxlength should be 20 characters long."
      },
      email: {
        required: "InValid Email",
        maxlength: "Your Email is invalid."
      },
      password: {
        required: "Please Enter Password",
        minlength: "Your password minlength should be 6 characters long."
      },
      mobile: {
        required: "Please Enter mobile",
        maxlength: "Your mobile maxlength should be 10 characters long."
      },
      profile: {
        required: "Invalid file"
      },
      gender: {
        required: "Please choice to Gender"
      },
      hobbies: {
        required: "Please check to Hobbies"
      },
      address: {
        required: "Please Enter Address"
      },
      city: {
        required: "Please Enter City"
      },
      state: {
        required: "Please Select State"
      },
    },
 
     submitHandler: function(form) {

      var actionType = $('#btn-save').val();
      $('#btn-save').html('Sending..');
      var form = $('#customerForm')[0];

      var data = new FormData(form);

      $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('customers.store') }}",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
          success: function (data) {
/*             $('#name').val(data.name);
            $('#email').val(data.email);
            $('#password').val(data.password);
            $('#mobile').val(data.mobile);
            $('#profile').val(data.profile);
            $('#gender').val(data.gender);
            $('#hobbies').val(data.hobbies);
            $('#address').val(data.address);
            $('#city').val(data.city);
            $('#state').val(data.state); */  
            var customer = '<tr id="customer_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.name + '</td><td>' + data.email + '</td><td>' + data.password + '</td><td>' + data.mobile + '</td><td>' + data.profile + '</td><td>' + data.gender + '</td><td>' + data.hobbies + '</td><td>' + data.address + '</td><td>' + data.city + '</td><td>' + data.state + '</td>';
            customer += '<td><a href="javascript:void(0)" id="edit-customer" data-id="' + data.id + '" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>';
            customer += '<td><a href="javascript:void(0)" id="delete-customer" data-id="' + data.id + '" class="btn btn-danger delete-customer"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>';
               
              
              if (actionType == "create-customer") {
                  $('#customers-crud').prepend(customer);
              } else {
                  $("#customer_id_" + data.id).replaceWith(customer);
              }
 
                $('#customerForm').trigger("reset");
                $('#ajax-crud-modal').modal('hide');
                $('#btn-save').html('Save Changes');  

              location.reload();
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn-save').html('Save Changes');
          }
      });
    }
  })
}
   
  
</script>