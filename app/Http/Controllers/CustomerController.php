<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Validator;

use App\User;

use Redirect,Response,File;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = User::latest()->paginate(5);
  
        return view('customer.index',compact('customers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'name' => 'required',
            'email' => 'required|email|unique:users,email|email:rfc,dns',
            'password' => 'required|string|min:6|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'gender' => 'required',
            'mobile' => 'required|numeric',
            'profile' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
            'hobbies' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required'
		]);
/* 
		$arr = array('msg' => 'Something went wrong. Please try again!', 'status' => false);
        if($validator->passes()){  */

            $files = $request->file('profile');
            $destinationPath = public_path('/profile_images/'); 
               
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
    
            $customerID = $request->customer_id;
               
            $customer   =   User::updateOrCreate(['id' => $customerID],
            ['name' => $request->name, 'email' => $request->email, 'password' => Hash::make($request->password), 'gender' => $request->gender, 'profile' => $profileImage, 'mobile' => $request->mobile, 'hobbies' => $request->hobbies, 'address' => $request->address, 'city' => $request->city, 'state' => $request->state]);
    
            return Response::json($customer); 
/*             $arr = array('msg' => 'Contact Added Successfully!', 'status' => true);
            return Response()->json($arr);
        }
        return Response()->json($arr); */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $customer  = User::where($where)->first();
 
        return Response::json($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = User::where('id',$id)->delete();
   
        return Response::json($customer);
    }
}
